import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { FrontFaceComponent } from './front-face.component'

const routes: Routes = [
  {
    path: '',
    component: FrontFaceComponent,
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrontFaceRoutingModule {}
