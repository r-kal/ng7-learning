import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ActiveComponent } from './active/active.component'
import { HistoryComponent } from './history/history.component'
import { MyTravelsComponent } from './my-travels.component'

const routes: Routes = [
  {
    path: 'my-travels',
    component: MyTravelsComponent,
    children: [
      {
        path: '',
        redirectTo: 'active',
        pathMatch: 'full',
      },
      {
        path: 'active',
        component: ActiveComponent,
      },
      {
        path: 'history',
        component: HistoryComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyTravelsRoutingModule {}
