import { Injectable } from '@angular/core'
import { TravelRequest } from '../../model'

@Injectable()
export class MyTravelsService {
  getActiveRequests(): TravelRequest[] {
    return [
      {
        id: 1,
        departureCity: 'Brno',
        destinationCity: 'azerbajdzan',
        dateFrom: new Date(2018, 8, 13),
        dateTo: new Date(2018, 8, 16),
        created: new Date(2018, 8, 22),
      },
    ]
  }
}
