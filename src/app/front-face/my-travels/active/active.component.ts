import { Component, OnInit } from '@angular/core'
import { TravelRequest } from 'src/app/model'
import { MyTravelsService } from '../my-travels.service'

@Component({
  selector: 'app-active',
  templateUrl: './active.component.html',
})
export class ActiveComponent implements OnInit {
  data: TravelRequest[]
  loading = true

  constructor(public myTravelsService: MyTravelsService) {}

  ngOnInit() {
    setTimeout(() => {
      this.data = this.myTravelsService.getActiveRequests()
      this.loading = false
    }, 1500)
  }
}
