import { LayoutModule } from '@angular/cdk/layout'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import {
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
} from '@angular/material'
import { LoadingSpinnerComponent } from '../shared/components/loading-spinner/loading-spinner.component'
import { ProjectionContentComponent } from '../shared/components/projection-content/projection-content.component'
import { FrontFaceRoutingModule } from './front-face-routing.module'
import { FrontFaceComponent } from './front-face.component'
import { MyTravelsService } from './my-travels'
import { ActiveComponent } from './my-travels/active/active.component'
import { HistoryComponent } from './my-travels/history/history.component'
import { MyTravelsRoutingModule } from './my-travels/my-travels-routing.module'
import { MyTravelsComponent } from './my-travels/my-travels.component'
import { DashboardComponent } from './request/dashboard/dashboard.component'
import { MyRequestListComponent } from './request/my-request-list/my-request-list.component'
import { ListComponent } from './shared/components/list/list.component'

@NgModule({
  imports: [
    FrontFaceRoutingModule,
    MyTravelsRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    LayoutModule,
    CommonModule,
  ],
  declarations: [
    FrontFaceComponent,
    MyRequestListComponent,
    ListComponent,
    DashboardComponent,
    ActiveComponent,
    HistoryComponent,
    MyTravelsComponent,
    LoadingSpinnerComponent,
    ProjectionContentComponent,
  ],
  providers: [MyTravelsService],
})
export class FrontFaceModule {}
