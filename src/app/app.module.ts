import { NgModule } from '@angular/core'
import { MatButtonModule, MatCheckboxModule } from '@angular/material'
import { MatCardModule } from '@angular/material/card'
import { MatSidenavModule } from '@angular/material/sidenav'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { HomeComponent } from './admin/home/home.component'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { PageNotFoundComponent } from './core/page-not-found/page-not-found.component'
import { FrontFaceModule } from './front-face/front-face.module'
import { SidebarComponent } from './shared/components/sidebar/sidebar.component'

@NgModule({
  declarations: [AppComponent, HomeComponent, PageNotFoundComponent, SidebarComponent],
  imports: [
    BrowserModule,
    MatButtonModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatSidenavModule,
    FrontFaceModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
