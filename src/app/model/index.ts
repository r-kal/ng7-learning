export interface GoalWithCommentsDto {
  goalId: number

  comments: string[]

  // /** enum GoalPriority */
  // priority: number;

  // /** enum GoalState */
  // state: number;

  // /** enum GoalType */
  // type: number;

  title: string
  // due: Date;
  // start: Date;
  description: string
  // bonusPeriodId: number;
  // targetMin: number;
  // target: number;
  // targetMax: number;
  // isTeam: boolean;
}

// TODO: Replace this with your own data model type
export interface TravelRequest {
  id: number

  departureCity: string
  destinationCity: string
  dateFrom: Date
  dateTo: Date
  created: Date
}
