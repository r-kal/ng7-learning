import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { HomeComponent } from './admin/home/home.component'
import { PageNotFoundComponent } from './core/page-not-found/page-not-found.component'

const routes: Routes = [
  {
    path: 'admin',
    component: HomeComponent,
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
