import { Component } from '@angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'my-app'

  avatarUrl =
    // tslint:disable-next-line:max-line-length
    'https://media.licdn.com/dms/image/C4D03AQG-_VfGqgXWNA/profile-displayphoto-shrink_100_100/0?e=1547683200&v=beta&t=E4C5OvXM9PzSPOb1x3n5yfTteqnkMJ1dg86Coh3oTyw'
}
