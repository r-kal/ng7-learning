import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-projection-content',
  // templateUrl: './projection-content.component.html',
  template: `
    <div>
      Name:
      <ng-content></ng-content>
    </div>
    <div><ng-content select="app-projection-content"></ng-content></div>
  `,
  styleUrls: ['./projection-content.component.scss'],
})
export class ProjectionContentComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
